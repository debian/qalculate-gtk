#!/bin/bash
# Sync the libqalculate-dev B-D version with debian/changelog
set -x
package_name=libqalculate-dev
target_version=">= $(dpkg-parsechangelog -S Version | sed 's/-.*/~/')"
sed -i -E "s/${package_name} \(>= ?\S+\)/${package_name} ($target_version)/" debian/control
if [[ ! $? ]]; then
    echo "no match"
else
    dch -a -m "Bump $package_name build-dependency to $target_version"
fi
